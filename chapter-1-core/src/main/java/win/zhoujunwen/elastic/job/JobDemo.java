package win.zhoujunwen.elastic.job;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

/**
 * Created with IntelliJ IDEA.
 * Date: 2017/10/11
 * Time: 上午9:41
 * Description:
 *
 * @author zhoujunwen
 * @version 1.0
 */
public class JobDemo {
    private static String zkHost = "localhost:2181";
    private static String namespace = "elastic-job-demo";
    private static String jobName = "demoSimpleJob";
    private static String cron = "0/15 * * * * ?";
    private static int shardingTotalCount = 10;

    public static void main(String[] args) {
        new JobScheduler(coordinatorRegistryCenter(), createJobConfiguration()).init();
    }

    // 注册中心配置
    private static CoordinatorRegistryCenter coordinatorRegistryCenter() {
        ZookeeperConfiguration configuration = new ZookeeperConfiguration(zkHost, namespace);
        CoordinatorRegistryCenter regCenter = new ZookeeperRegistryCenter(configuration);
        regCenter.init();
        return regCenter;
    }

    // 创建作业配置
    private static LiteJobConfiguration createJobConfiguration() {
        // 定义作业核心配置
        JobCoreConfiguration simpleCoreConfig = JobCoreConfiguration.newBuilder(jobName, cron, shardingTotalCount).build();
        // 定义SIMPLE类型配置
        SimpleJobConfiguration simpleJobConfig = new SimpleJobConfiguration(simpleCoreConfig, MyElasticJob.class.getCanonicalName());
        // 定义Lite作业根配置
        return LiteJobConfiguration.newBuilder(simpleJobConfig).build();
    }
}
