package win.zhoujunwen.elastic.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

/**
 * Created with IntelliJ IDEA.
 * Date: 2017/10/11
 * Time: 上午9:38
 * Description:
 *
 * @author zhoujunwen
 * @version 1.0
 */
public class MyElasticJob implements SimpleJob {
    public void execute(ShardingContext shardingContext) {
        switch (shardingContext.getShardingItem()) {
            case 0:
                // TODO: 2017/10/11
                System.out.println("000000");
                break;
            case 1:
                // TODO: 2017/10/11
                System.out.println("11111111");
                break;
            case 2:
                // TODO: 2017/10/11
                System.out.println("222222222");
                break;
            default:
                System.out.println(shardingContext.getShardingItem());
        }
    }
}
