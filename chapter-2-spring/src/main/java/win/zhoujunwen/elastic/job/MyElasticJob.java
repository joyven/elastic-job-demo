package win.zhoujunwen.elastic.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

/**
 * Created with IntelliJ IDEA.
 * Date: 2017/10/11
 * Time: 上午9:38
 * Description:
 *
 * @author zhoujunwen
 * @version 1.0
 */
public class MyElasticJob implements SimpleJob {
    public void execute(ShardingContext shardingContext) {
        switch (shardingContext.getShardingItem()) {
            case 0:
                System.out.println("分片1");
                break;
            case 1:
                System.out.println("分片2");
                break;
            case 2:
                System.out.println("分片3");
                break;
            default:
                System.out.println("分片" + shardingContext.getShardingItem() + 1);
        }
    }
}
